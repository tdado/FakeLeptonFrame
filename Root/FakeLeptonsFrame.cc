#include "FakeLeptonsFrame/FakeLeptonsFrame.h"

#include "FastFrames/DefineHelpers.h"
#include "FastFrames/UniqueSampleID.h"

#include "TVector2.h"

ROOT::RDF::RNode FakeLeptonsFrame::defineVariables(ROOT::RDF::RNode mainNode,
                                                   const std::shared_ptr<Sample>& sample,
                                                   const UniqueSampleID& /*id*/) {

  // You can also use the UniqueSampleID object to apply a custom defione
  // based on the sample:
  //   id.dsid() returns sample DSID
  //   id.campaign() returns sample campaign
  //   id.simulation() return simulation flavour
  // You can use it in your functions to apply only per sample define

  auto selectNjetNb = [](const std::vector<float>& jet_pt,
                         const std::vector<char>& select,
                         const std::size_t njets,
                         const std::size_t nbjets) {

    if (jet_pt.size() < njets) return false;

    std::size_t bjets(0);
    for (const auto iselect : select) {
      if (iselect) ++bjets;
    }

    return bjets >= nbjets;
  };

  auto selectLepton = [](const std::vector<float>& variable,
                         const std::vector<char>& pass,
                         const bool scale) -> double {

    if (variable.size() != 1) return -9999.;

    if (!pass.at(0)) return -9999.;

    return scale ? variable.at(0)/1e3 : std::abs(variable.at(0));

  };

  auto IFF = [](const std::vector<int>& iff,
                const std::vector<char>& pass) {

    if (iff.size() != 1) return -999;
    if (!pass.at(0)) return -999;

    return iff.at(0);
  };

  auto hasFakes = [](const std::vector<int>& el_iff,
                     const std::vector<int>& mu_iff) {

    for (const auto& el : el_iff) {
      // treat charge flips as prompt
      if (el != 2 && el != 3) return true;
    }
    for (const auto& mu : mu_iff) {
      if (mu != 4) return true;
    }

    return false;
  };

  auto Mwt = [](const std::vector<TLV>& el,
                const std::vector<TLV>& mu,
                const float met_et,
                const float met_phi) {

    const double pt = el.empty() ? mu.at(0).pt() : el.at(0).pt();
    const double phi = el.empty() ? mu.at(0).phi() : el.at(0).phi();
    const double dphi_lep_met = TVector2::Phi_mpi_pi(phi - met_phi);
    return std::sqrt(2 * pt * met_et * (1 - std::cos(dphi_lep_met)));
  };

  mainNode = systematicDefine(mainNode,
                              "mwt_NOSYS",
                              Mwt,
                              {"el_TLV_NOSYS", "mu_TLV_NOSYS", "met_met_NOSYS", "met_phi_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "select_4j1b_NOSYS",
                              [selectNjetNb](const std::vector<float>& jets, const std::vector<char>& select){return selectNjetNb(jets, select, 4, 1);},
                              {"jet_pt_NOSYS", "jet_GN2v01_FixedCutBEff_85_select"});

  mainNode = systematicDefine(mainNode,
                              "select_5j1b_NOSYS",
                              [selectNjetNb](const std::vector<float>& jets, const std::vector<char>& select){return selectNjetNb(jets, select, 5, 1);},
                              {"jet_pt_NOSYS", "jet_GN2v01_FixedCutBEff_85_select"});

  mainNode = systematicDefine(mainNode,
                              "select_4j2b_NOSYS",
                              [selectNjetNb](const std::vector<float>& jets, const std::vector<char>& select){return selectNjetNb(jets, select, 4, 2);},
                              {"jet_pt_NOSYS", "jet_GN2v01_FixedCutBEff_85_select"});

  mainNode = systematicDefine(mainNode,
                              "select_5j2b_NOSYS",
                              [selectNjetNb](const std::vector<float>& jets, const std::vector<char>& select){return selectNjetNb(jets, select, 5, 2);},
                              {"jet_pt_NOSYS", "jet_GN2v01_FixedCutBEff_85_select"});

  mainNode = systematicDefine(mainNode,
                              "el_pt_tight_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, true);},
                              {"el_pt_NOSYS", "el_select_tight_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "mu_pt_tight_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, true);},
                              {"mu_pt_NOSYS", "mu_select_tight_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "el_eta_tight_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, false);},
                              {"el_eta", "el_select_tight_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "mu_eta_tight_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, false);},
                              {"mu_eta", "mu_select_tight_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "el_pt_loose_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, true);},
                              {"el_pt_NOSYS", "el_select_loose_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "mu_pt_loose_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, true);},
                              {"mu_pt_NOSYS", "mu_select_loose_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "el_eta_loose_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, false);},
                              {"el_eta", "el_select_loose_NOSYS"});

  mainNode = systematicDefine(mainNode,
                              "mu_eta_loose_NOSYS",
                              [selectLepton](const std::vector<float>& lepton, const std::vector<char>& select){return selectLepton(lepton, select, false);},
                              {"mu_eta", "mu_select_loose_NOSYS"});


  if (!sample->isData()) {
    mainNode = systematicDefine(mainNode,
                                "el_IFF_tight_NOSYS",
                                IFF,
                                {"el_IFFClass", "el_select_tight_NOSYS"});

    mainNode = systematicDefine(mainNode,
                                "el_IFF_loose_NOSYS",
                                IFF,
                                {"el_IFFClass", "el_select_loose_NOSYS"});

    mainNode = systematicDefine(mainNode,
                                "mu_IFF_tight_NOSYS",
                                IFF,
                                {"mu_IFFClass", "mu_select_tight_NOSYS"});

    mainNode = systematicDefine(mainNode,
                                "mu_IFF_loose_NOSYS",
                                IFF,
                                {"mu_IFFClass", "mu_select_loose_NOSYS"});


    mainNode = systematicDefine(mainNode,
                                "hasFakes_NOSYS",
                                hasFakes,
                                {"el_IFFClass", "mu_IFFClass"});
  }

  return mainNode;
}

ROOT::RDF::RNode FakeLeptonsFrame::defineVariablesNtuple(ROOT::RDF::RNode mainNode,
                                                         const std::shared_ptr<Sample>& /*sample*/,
                                                         const UniqueSampleID& /*id*/) {

  return mainNode;
}

ROOT::RDF::RNode FakeLeptonsFrame::defineVariablesTruth(ROOT::RDF::RNode node,
                                                        const std::string& /*sample*/,
                                                        const std::shared_ptr<Sample>& /*sample*/,
                                                        const UniqueSampleID& /*sampleID*/) {
  return node;
}
