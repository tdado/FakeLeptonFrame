### Custom class for FastFrames

This is the repository of the code for processing ntuples from TopCPToolkit using [FastFrames](https://gitlab.cern.ch/atlas-amglab/fastframes) to get histgorams needed for real and fake lepton efficiencies for the Matrix Method aimed at 1L ttbar channel.
For compilation and other instructions, check the FastFrames documentation.
The code assumes that only nominal samples were produced (without systematic variations).
The code adds selection flags for 4j1b, 4j2b, 5j1b, 5j2b regions, pt and eta histograms needed for the efficiency estimate as well as the IFF class histograms.

A config for FastFrames is also provided: `config_fakes.yml`.
